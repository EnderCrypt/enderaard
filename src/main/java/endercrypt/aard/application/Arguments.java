package endercrypt.aard.application;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;


public class Arguments
{
	protected Arguments()
	{
		
	}
	
	@Option(name = "-version", aliases = { "--version", "-v" }, required = false, usage = "prints the version number rather than perform anything")
	private boolean version = false;
	
	public boolean isVersion()
	{
		return version;
	}
	
	@Option(name = "-help", aliases = { "--help", "-h" }, required = false, usage = "prints the help information rather than perform anything")
	private boolean help = false;
	
	public boolean isHelp()
	{
		return help;
	}
	
	@Argument
	private List<String> arguments = new ArrayList<>();
	
	public List<String> getArguments()
	{
		return Collections.unmodifiableList(arguments);
	}
}
