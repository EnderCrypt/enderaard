package endercrypt.aard.application;


import java.io.File;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.List;
import java.util.stream.Collectors;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import endercrypt.aard.Main;


public class Application
{
	private static Logger logger = LoggerFactory.getLogger(Application.class);
	
	public static final File filename = new File(Application.class.getProtectionDomain().getCodeSource().getLocation().getPath());
	
	public static final String name = "EnderAard";
	public static final String author = "EnderCrypt";
	public static final Version version = new Version(1, 0, 0);
	
	public static void main(String[] args) throws Exception
	{
		// launch arguments
		Arguments arguments = new Arguments();
		CmdLineParser parser = new CmdLineParser(arguments);
		try
		{
			parser.parseArgument(args);
		}
		catch (CmdLineException e)
		{
			System.err.println("Error: " + e.getMessage());
			System.err.println("java -jar " + filename.getName() + " [Options] [Files ...]");
			parser.printUsage(System.err);
			ExitCode.BAD_CMD_ARGS.endApplication();
		}
		
		// print version
		if (arguments.isVersion())
		{
			System.out.println(Application.class.getSimpleName() + " by " + Application.author + " Version " + Application.version);
			ExitCode.SUCCESS.endApplication();
		}
		
		// print help
		if (arguments.isHelp())
		{
			System.out.println("java -jar " + Application.filename.getName() + " [Options] [Files ...]");
			parser.printUsage(System.out);
			System.out.println();
			System.out.println("Exit Codes: ");
			List<ExitCode> exitCodes = ExitCode.getExitCodes().stream().sorted((e1, e2) -> Integer.compare(e1.getCode(), e2.getCode())).collect(Collectors.toList());
			for (ExitCode exitCode : exitCodes)
			{
				System.out.println("\t" + (exitCode.getCode() >= 0 ? " " : "") + exitCode.getCode() + " = " + exitCode.getDescription());
			}
			ExitCode.SUCCESS.endApplication();
		}
		
		// info
		logger.info(Application.name + " " + Application.version);
		
		// uncaught exception
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler()
		{
			@Override
			public void uncaughtException(Thread thread, Throwable exception)
			{
				String threadName = thread.getName();
				
				logger.error("Thread " + threadName + " crashed with uncaught exception", exception);
				ExitCode.UNCAUGHT_EXCEPTION.endApplication();
			}
		});
		
		// Main
		Main.main(arguments);
	}
}
