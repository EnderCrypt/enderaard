package endercrypt.aard.application;


import java.io.Serializable;

import org.apache.commons.text.WordUtils;


public class Version implements Serializable
{
	private static final long serialVersionUID = 7570800193611660738L;
	
	/**
	 * 
	 */
	
	private static final String SEPARATOR = ".";
	
	private static final String[] MAJOR_NAMES = {
		"In-Dev", // 0
		"Creation" // 1
	};
	
	private final int major;
	private final int minor;
	private final int patch;
	
	public Version(int major, int minor, int patch)
	{
		this.major = major;
		this.minor = minor;
		this.patch = patch;
		
		if (getMajor() > MAJOR_NAMES.length - 1)
		{
			throw new IllegalArgumentException("Major version " + getMajor() + " does not have a name");
		}
	}
	
	public int getMajor()
	{
		return major;
	}
	
	public int getMinor()
	{
		return minor;
	}
	
	public int getPatch()
	{
		return patch;
	}
	
	public String getName()
	{
		return WordUtils.capitalize(MAJOR_NAMES[getMajor()]).trim();
	}
	
	public String toVersionNumbers()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getMajor()).append(SEPARATOR);
		sb.append(getMinor()).append(SEPARATOR);
		sb.append(getPatch());
		return sb.toString();
	}
	
	@Override
	public String toString()
	{
		return toVersionNumbers() + " \"" + getName() + "\"";
	}
}
