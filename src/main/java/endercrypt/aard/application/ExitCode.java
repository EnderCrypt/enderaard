package endercrypt.aard.application;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ExitCode implements Comparable<ExitCode>
{
	private static Logger logger = LoggerFactory.getLogger(ExitCode.class);
	
	private static List<ExitCode> exitCodes = new ArrayList<>();
	
	public static final ExitCode SUCCESS = new ExitCode("The command executed successfully");
	
	public static final ExitCode UNIX_GENERAL_ERROR = new ExitCode("(UNIX) Catchall for general errors");
	public static final ExitCode UNIX_MISUSE_SHELL = new ExitCode("(UNIX) Misuse of shell builtins");
	
	public static final ExitCode BAD_CMD_ARGS = new ExitCode("Bad command-line arguments");
	public static final ExitCode UNCAUGHT_EXCEPTION = new ExitCode("Uncaught exception");
	
	static
	{
		Collections.sort(exitCodes);
	}
	
	private static int failureCodeCounter = 0;
	
	public static List<ExitCode> getExitCodes()
	{
		return Collections.unmodifiableList(exitCodes);
	}
	
	private int code;
	private String description;
	
	private ExitCode(String description)
	{
		this(failureCodeCounter++, description);
	}
	
	private ExitCode(int code, String description)
	{
		this.code = code;
		this.description = WordUtils.capitalize(description);
		
		exitCodes.add(this);
	}
	
	public int getCode()
	{
		return code;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + code;
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (!(obj instanceof ExitCode)) return false;
		ExitCode other = (ExitCode) obj;
		if (code != other.code) return false;
		return true;
	}
	
	@Override
	public int compareTo(ExitCode other)
	{
		return -Integer.compare(this.getCode(), other.getCode());
	}
	
	public void endApplication()
	{
		logger.info("Exit: ( " + getCode() + " ) " + getDescription());
		System.exit(getCode());
	}
}
